# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from web.models import Registration, About


class RegistrationAdmin(admin.ModelAdmin):
	list_display = ('id','name','email','phone','dob','education','message')

admin.site.register(Registration,RegistrationAdmin)

class AboutAdmin(admin.ModelAdmin):
	list_display = ('title','content','image')

admin.site.register(About,AboutAdmin)