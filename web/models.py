# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _

class Registration(models.Model):
    name = models.CharField(max_length=128)
    email = models.EmailField()
    phone = models.CharField(max_length=128)
    education = models.CharField(max_length=128)
    dob = models.DateField(blank=True,null=True)
    message = models.TextField()
                      
    class Meta:
    	db_table = 'web_registration'
    	verbose_name = _('registration')
    	verbose_name_plural = _('registrations')

    def _unicode_(self):
        return self.name

class About(models.Model):
    title = models.CharField(max_length=128)
    content = models.TextField()
    image = models.ImageField(upload_to="web/about/")
    subheading = models.CharField(max_length=128) 
    date_added = models.DateField()

    class Meta:
        db_table = 'web-about'
        verbose_name = "about"
        verbose_name_plural = "about"

    def _unicode_(self):
        return self.title
   