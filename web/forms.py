from django import forms
from django.forms.widgets import TextInput, Textarea
from django.utils.translation import ugettext_lazy as _
from web.models import Registration


class RegistrationForm(forms.ModelForm):

	class Meta:
		model = Registration
		fields = '__all__'
		widgets = {
			'name':TextInput(attrs={'class': 'required', 'placeholder': 'Name'}),
			'email':TextInput(attrs={'class': 'required', 'placeholder': 'Email'}),
			'phone':TextInput(attrs={'class': 'required', 'placeholder': 'Phone'}),
			'education':TextInput(attrs={'class': 'required', 'placeholder': 'Education'}),
			'dob':Textarea(attrs={'class': 'required', 'placeholder': 'Dob'}),
			'message':TextInput(attrs={'placeholder': 'Message'}),
		}
		error_messages = {
			'name':{
				'required':_("name fielsd is required."),
				},
			'phone':{
				'required':_("phone field is required."),
				},
			'email':{
				'required':_("email fielsd is required."),
				},
			'education':{
				'required':_("education fielsd is required."),
				},
			'message':{
				'required':_("message fielsd is required."),
				}

		} 
		labels = {
			"email" : "enter your E-mail",
			"message" : "what is on your mind"
		}