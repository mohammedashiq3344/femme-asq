   
from django.conf.urls import url
import views

urlpatterns = [
    url(r'^$',views.index, name="index"),
    url(r'^about/$',views.about, name="about"),
    url(r'^registration/$',views.registration, name="registration"),
]
 