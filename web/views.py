       
from __future__ import unicode_literals
from django.shortcuts import render
from django.http.response import HttpResponse,HttpResponseRedirect
from django.core.urlresolvers import reverse
from web.models import About, Registration
from web.forms import RegistrationForm
from web.functions import generate_form_errors
import json

def index(request):
    about_datas =  About.objects.all()
    form = RegistrationForm()
    context = {
        "title": "FEMME",
        "caption": "femme caption",
        "about_datas":about_datas,
        "form" : form,
        "is_home" : True
   
    }
    return render(request,'web/index.html',context)


def about(request):
    about_datas =  About.objects.all()
    context = {
    	"title": "about",
    	"about_datas":about_datas,
        "is_about" : True
   
    }
    return render(request,'web/about.html',context)


def registration(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully created",
                "message" : "Registration Successfully Updated.",
               
            }
        else:
            message = generate_form_errors(form,formset=False)     

            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        return HttpResponse("invalid request")
